Description: add a simple use test
 We don't run the network based tests, but should have something that actually
 loads the package's module.
Author: Florian Schlichting <fsfs@debian.org>
Forwarded: not-needed

--- /dev/null
+++ b/t/01use.t
@@ -0,0 +1,4 @@
+use strict;
+use Test::More;
+require_ok('Math::RandomOrg');
+done_testing;
